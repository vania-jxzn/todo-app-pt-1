import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    inputText: "",
  };

  
  onChange = (e) =>{
    this.setState({[e.target.name]: e.target.value})
  }

  addTDitem = (e) => {
    if(e.keyCode === 13){
      const newTodo = this.state.todos
      const newObj = {
        "userId": 1,
        "id": 1,
        "title": this.state.inputText,
        "completed": false
      };
      newTodo.push(newObj);
      this.setState({todos: newTodo, inputText: ""});
    }
  }

  toggCompleted = (id) => (e) => {
    const newTodos = this.state.todos;

    newTodos.forEach((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
    });

    this.setState({ todos: newTodos });
  };

  
  handleDelete = (id) => (e) => {
    const newTodos = this.state.todos.filter(todo => todo.id !== id );

    this.setState({todos: newTodos});
  };

  clearTDitems = () => {
    const newTodos = this.state.todos.filter(todo => todo.completed === false)

    this.setState({todos: newTodos})
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          className="new-todo" 
          placeholder="What needs to be done?" 
          autofocus
          name="inputText"
          onChange={this.onChange} 
          onKeyDown={(e)=>this.addTDitem(e)}
          value={this.state.inputText}
          />
        </header>
        <TodoList 
        todos={this.state.todos}
        toggCompleted={this.toggCompleted}
        handleDelete={this.handleDelete}  
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearTDitems}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {


  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          className="toggle" 
          type="checkbox" 
          checked={this.props.completed}   
          onClick={this.props.toggCompleted}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDelete} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
            title={todo.title} 
            completed={todo.completed} 
            toggCompleted={this.props.toggCompleted(todo.id)} 
            handleDelete={this.props.handleDelete(todo.id)} 
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
